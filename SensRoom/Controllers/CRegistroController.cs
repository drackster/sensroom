﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SensRoom.Controllers
{
    public class CRegistroController : Controller
    {
        // GET: CRegistro
        public ActionResult Index()
        {
            return View();
        }

        // GET: CRegistro/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: CRegistro/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CRegistro/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CRegistro/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: CRegistro/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: CRegistro/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: CRegistro/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}