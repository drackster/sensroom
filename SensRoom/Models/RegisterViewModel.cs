﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SensRoom.Models
{
    public class RegisterViewModel
    {
        public string name { get; set; }
        public string pass { get; set; }
        public string confPass { get; set; }
        public bool tyc { get; set; }
        public string error { get; set; }
    }
}
