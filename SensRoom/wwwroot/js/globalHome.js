"use strict";
$(function () {
   
	const tabItems = document.querySelectorAll('.tab-item');
	const tabContentItems = document.querySelectorAll('.tab-content-item');

	// Select tab content item
	function selectItem(e) {
		// Remove all show and border classes
		removeBorder();
		removeShow();
		// Add border to current tab item
		this.classList.add('tab-border');
		// Grab content item from DOM
		const tabContentItem = document.querySelector(`#${this.id}-content`);
		// Add show class
		tabContentItem.classList.add('show');
	}

	// Remove bottom borders from all tab items
	function removeBorder() {
		tabItems.forEach(item => {
			item.classList.remove('tab-border');
		});
	}

	// Remove show class from all content items
	function removeShow() {
		tabContentItems.forEach(item => {
			item.classList.remove('show');
		});
	}

	// Listen for tab item click
	tabItems.forEach(item => {
		item.addEventListener('click', selectItem);
	});
    
});

// Loader
$(function (window, document, $, undefined) {

	jQuery(function () {
		jQuery(".loader").fadeOut('slow');
	});

});

// Plugin Smooth Scroll 
$(function () {
	var e = '[data-toggle="smooth-scroll"]';
	var scroll2 = new SmoothScroll('a[href*="#"]', {
		speed: 1500
	});
	"undefined" != typeof SmoothScroll && new SmoothScroll(e, {
		header: ".navbar.fixed-top",
		offset: function (e, t) {
			return t.dataset.offset ? t.dataset.offset : 24
		}
	})
});